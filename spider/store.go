package spider

import (
	"encoding/hex"
	"fmt"
	"time"

	"gitlab.com/btlike/crawl/utils"
)

type torrent struct {
	Name       string
	Infohash   string
	Length     int64
	FileCount  int64
	Heat       int64
	Files      []file
	CreateTime time.Time
}

type file struct {
	Name   string
	Length int64
}

func exist(key string) bool {
	searchResult, err := utils.ES.Get().Index("torrent").Type("infohash").Id(key).Do()
	if err == nil && searchResult != nil && searchResult.Found {
		return true
	}
	return false
}

func storeTorrent(data interface{}, infohash []byte) (err error) {
	manage.storeCount++
	defer func() {
		e := recover()
		if e != nil {
			err = fmt.Errorf("%v", e)
		}
	}()

	var t torrent
	t.CreateTime = time.Now()
	ihStr := hex.EncodeToString(infohash)
	if exist(ihStr) {
		return
	}

	if info, ok := data.(map[string]interface{}); ok {
		//get name
		if name, ok := info["name"].(string); ok {
			t.Name = name
			if t.Name == "" {
				return fmt.Errorf("store name len is 0")
			}
		}
		//get infohash
		t.Infohash = ihStr
		if len(t.Infohash) != 40 {
			return fmt.Errorf("store infohash len is not 40")
		}
		//get files
		if v, ok := info["files"]; !ok {
			t.Length = int64(info["length"].(int))
			t.FileCount = 1
			t.Files = append(t.Files, file{Name: t.Name, Length: t.Length})
		} else {
			fls := v.([]interface{})
			for _, item := range fls {
				fl := item.(map[string]interface{})
				flName := fl["path"].([]interface{})

				length := int64(fl["length"].(int))
				t.Length += length
				t.FileCount++
				t.Files = append(t.Files, file{
					Name:   flName[0].(string),
					Length: length,
				})
			}
		}

		_, err = utils.ES.Index().
			Index("torrent").
			Type("infohash").
			Id(t.Infohash).
			BodyJson(t).
			Refresh(false).
			Do()

	}
	return
}
